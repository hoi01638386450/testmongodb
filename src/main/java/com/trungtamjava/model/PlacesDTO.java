package com.trungtamjava.model;

import java.time.LocalDate;
import java.util.ArrayList;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;

import com.trungtamjava.entity.File;

import lombok.Data;

@Data
public class PlacesDTO {
	private ObjectId _id;

	private String country_code;

	private ObjectId region;

	private String name;

	private String address;

	private String short_description;

	private String tel;

	private String website;

	private String email;

	private ArrayList<ObjectId> categories;

	private String collections;

	@DBRef
	private File files;

	private ArrayList<String> tags;

	private int status;

	private ObjectId user;

	private LocalDate created;

	private LocalDate modified;

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public ObjectId getObjectId() {
		return _id;
	}

}
