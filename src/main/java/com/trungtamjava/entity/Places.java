package com.trungtamjava.entity;

import java.time.LocalDate;
import java.util.ArrayList;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(value = "places")
@Data
public class Places {
	@Id
	private ObjectId _id;

	private String country_code;

	private ObjectId region;

	private String name;

	private String address;

	private String short_description;

	private String tel;

	private String website;

	private String email;

	private ArrayList<ObjectId> categories;

	private String collections;

	@DBRef
	private File files;

	private ArrayList<String> tags;

	private int status;

	private ObjectId user;

	private LocalDate created;

	private LocalDate modified;

	
}
