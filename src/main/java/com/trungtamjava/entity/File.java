package com.trungtamjava.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(value = "places")
@Data
public class File {
	private String banner;

	private String logo;

	private String thmbnails;

	private String video;
}
