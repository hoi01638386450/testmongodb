package com.trungtamjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class testMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(testMongoApplication.class, args);
	}

}
