package com.trungtamjava.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.trungtamjava.entity.Places;
@Repository
public interface PlacesRepository extends MongoRepository<Places, ObjectId> {
	
	 Places  findBy_id(ObjectId _id);;

}
