package com.trungtamjava.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.trungtamjava.entity.Places;
import com.trungtamjava.model.PlacesDTO;
import com.trungtamjava.repository.PlacesRepository;
import com.trungtamjava.service.PlacesService;

@RestController

public class PlacesController {

	@Autowired
	PlacesService placesService;

	@Autowired
	PlacesRepository placesRepository;

	@GetMapping(value = "/places")
	public List<PlacesDTO> placesDTOs(HttpServletRequest request) {
		List<PlacesDTO> placesDTOs = placesService.getAllPlaces();

		return placesDTOs;
	}

	@GetMapping(value = "/places/{id}")
	public PlacesDTO placesById(@PathVariable(name="id") ObjectId id ) {
		PlacesDTO placesDTO =placesService.findBy_id(id);
		return placesDTO; 
	}
	@PostMapping(value= "/places/add")
	public Places addPlaces(@RequestBody PlacesDTO placesDTO ) {
	 return placesService.addPlaces(placesDTO);
	 }
	@DeleteMapping(value = "/places/delete/{id}")
	public void deletePlaces( @PathVariable("id") ObjectId id) {
		placesService.deletetPlaces(id);
		}
}
