package com.trungtamjava.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.trungtamjava.entity.Places;
import com.trungtamjava.model.PlacesDTO;
import com.trungtamjava.repository.PlacesRepository;
import com.trungtamjava.service.PlacesService;

@Configuration
public class PlacesServiceImpl implements PlacesService {
	@Autowired
	private PlacesRepository placesRepository;

	@Override
	public List<PlacesDTO> getAllPlaces() {
		List<Places> places = placesRepository.findAll();
		List<PlacesDTO> placesDTOs = new ArrayList<PlacesDTO>();
		for (Places p : places) {
			PlacesDTO dto = new PlacesDTO();
			dto.set_id(p.get_id());
			dto.setCountry_code(p.getCountry_code());
			dto.setRegion(p.getRegion());
			dto.setName(p.getName());
			dto.setAddress(p.getAddress());
			dto.setShort_description(p.getShort_description());
			dto.setTel(p.getTel());
			dto.setWebsite(p.getWebsite());
			dto.setEmail(p.getEmail());
			dto.setCategories(p.getCategories());
			dto.setCollections(p.getCollections());
			dto.setFiles(p.getFiles());
			dto.setTags(p.getTags());
			dto.setStatus(p.getStatus());
			dto.setUser(p.getUser());
			dto.setCreated(p.getCreated());
			dto.setModified(p.getModified());
			placesDTOs.add(dto);

		}
		return placesDTOs;
	}

	@Override
	public Places addPlaces(PlacesDTO placesDTO) {
		Places places = new Places();
		places.setCountry_code(placesDTO.getCountry_code());
		places.setRegion(placesDTO.getRegion());
		places.setName(placesDTO.getName());
		places.setAddress(placesDTO.getAddress());
		places.setShort_description(placesDTO.getShort_description());
		places.setTel(placesDTO.getTel());
		places.setWebsite(placesDTO.getWebsite());
		places.setEmail(placesDTO.getEmail());
		places.setCategories(placesDTO.getCategories());
		places.setCollections(placesDTO.getCollections());
		places.setFiles(placesDTO.getFiles());
		places.setTags(placesDTO.getTags());
		places.setStatus(placesDTO.getStatus());
		places.setUser(placesDTO.getUser());
		places.setCreated(placesDTO.getCreated());
		places.setModified(placesDTO.getModified());
		placesRepository.save(places);
		return places;

	}

	@Override
	public void deletetPlaces(ObjectId id) {
		placesRepository.deleteById( id);

	}

	@Override
	public PlacesDTO findBy_id(ObjectId id) {
		
		Places p = placesRepository.findBy_id(id);
		PlacesDTO dto = new PlacesDTO();
		dto.set_id(p.get_id());
		dto.setCountry_code(p.getCountry_code());
		dto.setRegion(p.getRegion());
		dto.setName(p.getName());
		dto.setAddress(p.getAddress());
		dto.setShort_description(p.getShort_description());
		dto.setTel(p.getTel());
		dto.setWebsite(p.getWebsite());
		dto.setEmail(p.getEmail());
		dto.setCategories(p.getCategories());
		dto.setCollections(p.getCollections());
		dto.setFiles(p.getFiles());
		dto.setTags(p.getTags());
		dto.setStatus(p.getStatus());
		dto.setUser(p.getUser());
		dto.setCreated(p.getCreated());
		dto.setModified(p.getModified());
		return dto;

	}

	@Override
	public void updatePlaces(PlacesDTO placesDTO) {
//		Places places = placesRepository.findBy_id(placesDTO.getObjectId());
		

	}

}
