package com.trungtamjava.service;

import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;

import com.trungtamjava.entity.Places;
import com.trungtamjava.model.PlacesDTO;

public interface PlacesService {
	public List<PlacesDTO> getAllPlaces();

	public Places addPlaces(PlacesDTO placesDTO);

	public void deletetPlaces(ObjectId id);

	public void updatePlaces(PlacesDTO placesDTO);

	public PlacesDTO findBy_id(ObjectId id);

}
